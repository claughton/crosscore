dask_initial_config = {
    "scheduler_url": None
}

adaptive_initial_config = {
    "max_workers": 1,
    "max_idle_time": 300,
    "poll_interval": 10
}

terraform_initial_config = {
    "cloud_provider": "azure",
    "image_name": "XcoreJobrunnerImage",
    "scheduler_name": "scheduler",
    "scheduler_type": "Standard_B2ms",
    "scheduler_port": "8786",
    "dashboard_port": "8787",
    "worker_names": [],
    "worker_type": "Standard_B2ms",
    "subscription": None,
    "public_key_path": None,
    "location": "UK South",
    "user_name": None
}

terraform_template = """variable "subscription"  {
  type = string
  description = "Azure subscription name"
}

variable "location" {
  type = string
  description = "Azure location to use"
}

variable "cloud_provider" {
  type = string
  description = "Cloud provider - not used in template"
}

variable "image_name" {
  type = string
  description = "image name used to create instances"
}

variable "scheduler_name" {
  type = string
  description = "name for scheduler instance to launch"
  default = "scheduler"
}

variable "scheduler_type" {
  type = string
  description = "type of scheduler instance to launch"
  default = "f1-micro"
}

variable "scheduler_port" {
  type = string
  description = "port used by dask scheduler"
  default = "8786"
}

variable "dashboard_port" {
  type = string
  description = "port used by dask dashboard"
  default = "8787"
}

variable "worker_type" {
  type = string
  description = "type of worker instance to launch"
  default = "f1-micro"
}

variable "worker_names" {
  type = list
  description = "names of instances to launch"
  default = []
}

variable "user_name" {
  type = string
  description = "user name on local machine"
}

variable "public_key_path" {
  type = string
  description = "path to public key file"
}

variable "resource_group_name" {
  type = string
  description = "name of Azure resource group"
}

provider "azurerm" {
  features {}
  subscription_id = var.subscription
  skip_provider_registration = true
}

data "azurerm_resource_group" "rg" {
  name     = var.resource_group_name
}

resource "azurerm_virtual_network" "vnet" {
  name                = "vnet"
  address_space       = ["10.0.0.0/16"]
  location            = data.azurerm_resource_group.rg.location
  resource_group_name = data.azurerm_resource_group.rg.name
}

resource "azurerm_subnet" "snet" {
  name                 = "subnetname"
  resource_group_name  = data.azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = ["10.0.2.0/24"]
  service_endpoints    = ["Microsoft.Sql", "Microsoft.Storage"]
}

resource "azurerm_public_ip" "schedulerpublicip" {
    name                         = "schedulerPublicIP"
    location                     = data.azurerm_resource_group.rg.location
    resource_group_name          = data.azurerm_resource_group.rg.name
    allocation_method            = "Dynamic"
}


resource "azurerm_network_security_group" "nsg" {
    name                = "NetworkSecurityGroup"
    location            = data.azurerm_resource_group.rg.location
    resource_group_name = data.azurerm_resource_group.rg.name

    security_rule {
        name                       = "SSH"
        priority                   = 1001
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "22"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }

    security_rule {
        name                       = "scheduler"
        priority                   = 1000
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_ranges    = [var.scheduler_port, var.dashboard_port]
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }
}

resource "azurerm_network_interface" "scheduler-nic" {
  name                = "scheduler-nic"
  location            = data.azurerm_resource_group.rg.location
  resource_group_name = data.azurerm_resource_group.rg.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.snet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.schedulerpublicip.id
  }
}

# Connect the security group to the network interface
resource "azurerm_network_interface_security_group_association" "scheduler" {
    network_interface_id      = azurerm_network_interface.scheduler-nic.id
    network_security_group_id = azurerm_network_security_group.nsg.id
}

data "azurerm_image" "image" {
  name                = var.image_name
  resource_group_name = data.azurerm_resource_group.rg.name
}

resource "azurerm_linux_virtual_machine" "scheduler" {
  name                = var.scheduler_name
  resource_group_name = data.azurerm_resource_group.rg.name
  location            = data.azurerm_resource_group.rg.location
  size                = var.scheduler_type
  admin_username      = "adminuser"
  network_interface_ids = [
    azurerm_network_interface.scheduler-nic.id,
  ]

  admin_ssh_key {
    username   = "adminuser"
    public_key = file(var.public_key_path)
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_id = data.azurerm_image.image.id

  custom_data = base64encode(<<-EOF
       #! /bin/sh
        sudo apt update
        sudo apt install -y python3-pip 
        sudo ubuntu-drivers autoinstall
        sudo pip3 install dask>=2021.3.0 distributed>=2021.3.0 bokeh crossflow==0.0.4
        sudo /usr/local/bin/dask-scheduler --port "${var.scheduler_port}" --dashboard-address ":${var.dashboard_port}"> /tmp/scheduler.log 2>&1 &
  EOF
  )

  boot_diagnostics {
        storage_account_uri = azurerm_storage_account.storacc.primary_blob_endpoint
    }
}

resource "azurerm_storage_account" "storacc" {
  name                = "xcorestoracc"
  resource_group_name = data.azurerm_resource_group.rg.name

  location                 = data.azurerm_resource_group.rg.location
  account_tier             = "Standard"
  account_replication_type = "LRS"

  network_rules {
    default_action             = "Deny"
    ip_rules                   = ["100.0.0.1"]
    virtual_network_subnet_ids = [azurerm_subnet.snet.id]
  }
}

resource "azurerm_network_interface" "worker-nic" {
  for_each            = toset(var.worker_names)
  name                = "${each.key}-nic"
  location            = data.azurerm_resource_group.rg.location
  resource_group_name = data.azurerm_resource_group.rg.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.snet.id
    private_ip_address_allocation = "Dynamic"
  }
}

# Connect the security group to the network interface
resource "azurerm_network_interface_security_group_association" "worker" {
    for_each                  = toset(var.worker_names)
    network_interface_id      = azurerm_network_interface.worker-nic[each.key].id
    network_security_group_id = azurerm_network_security_group.nsg.id
}

resource "azurerm_linux_virtual_machine" "worker" {
  for_each = toset(var.worker_names)
  name = each.key
  size = var.worker_type
  resource_group_name = data.azurerm_resource_group.rg.name
  location            = data.azurerm_resource_group.rg.location
  admin_username      = "adminuser"
  network_interface_ids = [
    azurerm_network_interface.worker-nic[each.key].id,
  ]

  admin_ssh_key {
    username   = "adminuser"
    public_key = file(var.public_key_path)
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_id = data.azurerm_image.image.id

  custom_data = base64encode(<<-EOF
        #! /bin/sh
        sudo apt update
        sudo apt install -y python3-pip
        sudo ubuntu-drivers autoinstall
        sudo pip3 install dask>=2021.3.0 distributed>=2021.3.0 crossflow==0.0.3rc2
        sudo /usr/local/bin/dask-worker --name "${each.key}" --nthreads 1 --local-directory /tmp/dask-worker-dir "${azurerm_linux_virtual_machine.scheduler.private_ip_address}:${var.scheduler_port}"  > /tmp/dask-worker.log 2>&1 &
  EOF
  )

  boot_diagnostics {
        storage_account_uri = azurerm_storage_account.storacc.primary_blob_endpoint
    }
}

// A variable for extracting the external ip of the scheduler
output "scheduler-url" {
  value = "${azurerm_linux_virtual_machine.scheduler.public_ip_address}:${var.scheduler_port}"
}

// A variable for extracting the external ip of the workers
output "worker-ip" {
  value = values(azurerm_linux_virtual_machine.worker)[*].public_ip_address
}
"""