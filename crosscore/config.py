import os
import os.path as op
import yaml
#
# Operations on the Crosscore configuration file
#
homedir = os.getenv('HOME')
configdir = op.join(homedir, '.xcore')
configfile = op.join(configdir, 'config.yaml')

def check_config():
    if not op.exists(configdir):
        raise RuntimeError(f'Error: cannot find configdir {configdir}. Have you run '
                           'xcore-init yet?')
    if not op.exists(configfile):
        raise RuntimeError(f'Error: cannot find config file at {configfile}. Have you run '
                           f'xcore-init yet?')

def load_config():
    check_config()
    with open(configfile) as f:
        config = yaml.load(f, Loader=yaml.SafeLoader)
    return config

def save_config(config):
    with open(configfile, 'w') as f:
        yaml.dump(config, f)
