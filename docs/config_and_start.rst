**************************************
Configuration and starting the cluster
**************************************

=============
Configuration
=============
Produce an initial default configuration for ``xcore`` using the command::

    xcore-init <provider>

where ``<provider>`` is ``aws``, ``azure``, or ``gcp``.

This will run some checks that you have all the prerequisites, and create some
configuration and template files. These will be placed in ``$HOME/.xcore``.

Once complete, check the default configuration in ``$HOME/.xcore/config.yaml``. In
particular you may want to change ``max_workers`` - the maximum number of concurrent
worker nodes you will allow, and the name of the image generated by packer in the
``image_name`` and associated ``image_owner`` fields.

Although you can hand-edit the configuration file, you can also set parameters with
the ``xcore configure`` command, e.g.::

    xcore configure max_workers=4

======================
Start the xcore system
======================
Once you are happy with the configuration, run ``xcore start`` to create the base cloud
infrastructure and launch the *Crosscore* daemon. The base infrastructure consists of
a small (default t2.small/f1-micro/Standard_B2ms) instance that runs the scheduler, the
daemon listens for job requests and autoscales the cluster as required.

This command may take some time to run.
