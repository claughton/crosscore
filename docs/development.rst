***********
Development
***********

This information is for developers of the Crosscore package.

To rebuild the documentation (for display at `ReadTheDocs <https://crosscore.readthedocs.io/en/latest/index.html>`_),
pip install ``Sphinx`` and ``sphinxcontrib-autoprogram``, and then run the following command from the ``docs/``
directory::

    sphinx-apidoc -t ./_templates/ -o ./_modules ../crosscore ../crosscore/aws ../crosscore/az ../crosscore/gcp ../crosscore/terraform.py ../crosscore/config.py  && sphinx-build -b html . _build/ && make html

and then it is probably sufficient to commit changes from the ``docs/`` and ``docs/_modules`` directories.