***********************
Running your first jobs
***********************

This page offers some guidance on running your first jobs. More detailed instructions
on the usage of all of the ``xcore`` commands can be found in :doc:`Scripts <scripts>`.

==============
Run a test job
==============
Create a small *crossflow* workflow, e.g.::

    from crossflow.kernels import SubprocessKernel
    from crossflow.clients import Client
    from crosscore import cluster

    sleeper = SubprocessKernel('sleep {n}; echo {n}')
    sleeper.set_inputs(['n'])
    sleeper.set_outputs(['STDOUT'])

    client = Client(address=cluster.get_url())
    result = client.submit(sleeper, 10)
    print(result.result())

If you run this Python script interactively in one window, you can use
``xcore status`` from another to follow the process of worker creation, the
job being run, and the worker being deleted after.

=====================
Shut down the cluster
=====================
If you are not going to use the cluster for a while, you can shut down the
scheduler instance and stop the daemon::

    xcore shutdown

When you want to use it again, you run ``xcore restart``

===========================================
Changing the instance type and cluster size
===========================================
Within a script you can adjust the maximum number of instances that may be
launched, and their instance type, before you submit the job, e.g.::

    ...
    # AWS example:
    cluster.set_worker_type('c5.xlarge')
    # Azure example:
    # cluster.set_worker_type('Standard_B2ms')
    # GCP example:
    # cluster.set_worker_type('n1-standard-4', accelerator_type='nvidia-tesla-t4')
    cluster.set_max_workers(5)
    client = Client(cluster.get_url())
    ...

==========================
Changing the machine image
==========================
The workflows you can run using *crossflow* depends on the software installed
on your worker nodes. Though you may be able to do some provisioning of these
on the fly (i.e., within *crossflow* kernel definitions) most likely you will
want to prepare machine images with your favourite software stack pre-installed.
Examples of how this can be done using `Packer <https://www.packer.io>`_ are
available in the ``packer`` folder.

Note that if you change the machine image, you will need to restart *crosscore*
(``crosscore shutdown; crosscore restart``).
