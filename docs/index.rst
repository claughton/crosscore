.. Crossqueue documentation master file, created by
   sphinx-quickstart on Tue Mar 30 12:03:43 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

#########
Crosscore
#########

*Crosscore* is part of `Project Crossbow <https://bitbucket.org/claughton/crossbow>`_. It
allows you to create an autoscaling pool of instances in the cloud that can then be used with
`crossflow <https://bitbucket.org/claughton/crossflow>`_ to execute computational workflows.


Currently *Crosscore* supports `Amazon Web Services <https://aws.amazon.com>`_,
`Microsoft Azure <https://azure.microsoft.com>`_ and `Google Cloud Platform <https://cloud.google.com>`_.

Documentation can be found at `ReadTheDocs <https://crosscore.readthedocs.io/en/latest/>`_.

********
 Authors
********
* Christian Suess
* Charlie Laughton charles.laughton@nottingham.ac.uk
* Sam Cox

***************
Acknowlegements
***************
EPSRC Grant `EP/P011993/1 <https://gow.epsrc.ukri.org/NGBOViewGrant.aspx?GrantRef=EP/P011993/1>`_


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   config_and_start
   first_job
   packer
   api
   scripts
   development

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
