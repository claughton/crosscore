************
Installation
************
=============
Prerequisites
=============
--------------
Python Version
--------------
*Crosscore* requires Python 3.7 or higher. No version of Python 2 is supported.

-------------
Configuration
-------------
*Crosscore* supports AWS, Azure, and GCP. Depending on which of these providers
you plan to use, some preliminary configuration is required:

^^^
AWS
^^^
It is assumed that you have done what is required to give you programmatic access
to your AWS account. This will involve generating your *AWS AccessKey ID* and
*Secret Access Key*, and installing them with ``aws configure``.

In addition you need to make sure your account has the following permissions::

    Amazon EC2FullAccess

^^^^^
Azure
^^^^^
Log in with the Azure CLI to your Azure account, setting the active subscription
to the correct id. It is assumed that you have an existing resource group
``xcore-resource-group`` containing a machine image called ``XcoreJobrunnerImage``.
This image can be created by following the instructions in :doc:`Packer <packer>`.
To point to another resource group, set the environment variable::

    export AZURE_RESOURCE_GROUP_NAME=<name of resource group>

^^^
GCP
^^^
You need to have downloaded a .json file with your service account credentials - see
`here <https://cloud.google.com/iam/docs/creating-managing-service-account-keys>`_
for details. Then you need to decide on an availability zone for your cluster - bear
in mind that this will affect the range of instance types (particularly GPU
accelerators) you will be able to launch. With these in hand, create two environment
variables::

    export GOOGLE_APPLICATION_CREDENTIALS=<path to credentials file>
    export GOOGLE_DEFAULT_AVAILABILITY_ZONE=<availability zone>

Finally, ensure that you have enabled the Compute Engine API for the project named in
your service account credentials.

---------
Terraform
---------
*Crosscore* uses `Terraform <https://www.terraform.io>`_ to do the heavy lifting of
cloud infrastructure creation and management. Before you can use *Crosscore* you
must install terraform according to their instructions. Once you can run ::

    terraform -version

you have done enough.

---
SSH
---
You will need an ssh public key (e.g., ``$HOME/.ssh/id_rsa.pub``). If you don't
already have this, use ``ssh-keygen`` to make it.

Create another environment variable with the location of this file::

    export SSH_PUBLIC_KEY=<path to id_rsa.pub or equivalent>

====================================
Install the Crosscore Python Package
====================================
*Crosscore* is not currently in `pypi <https://pypi.org>`_ so to install it use::

    pip install git+https://bitbucket.org/claughton/crosscore.git

If all goes smoothly, you can then check the installation is OK by running ``xcore -h``::

    usage: xcore [-h] [-V] {status,start,restart,shutdown,daemon} ...

    Crosscore: Cloud clusters for distributed computing.

    optional arguments:
      -h, --help            show this help message and exit
      -V, --version         show program's version number and exit

    subcommands:
      {status,start,restart,shutdown,daemon}
        status              status of crosscore cluster
        start               create cloud resources
        restart             recreate cloud resources
        shutdown            terminate and delete all resources
        daemon              control the xcore daemon
