=======
Scripts
=======

**************
Initialisation
**************
.. autoprogram:: xcore-init:parser
   :prog: xcore-init

*********************
The ``xcore`` command
*********************
.. autoprogram:: xcore:parser
   :prog: xcore
