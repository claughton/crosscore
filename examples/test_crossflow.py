from crossflow.kernels import SubprocessKernel
from crossflow.clients import Client
from crosscore import cluster

sleeper = SubprocessKernel('sleep {n}; echo {n}')
sleeper.set_inputs(['n'])
sleeper.set_outputs(['STDOUT'])

client = Client(address=cluster.get_url())
result = client.submit(sleeper, 10)
print(result.result())
