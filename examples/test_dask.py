from distributed import Client
from crosscore import cluster

client = Client(address=cluster.get_url())
def adder(a, b):
    return a + b
result = client.submit(adder, 5, 6)
print(result.result())
