######
Packer
######
The Crosscore package contains `Packer <https://www.packer.io>`_ files to build images for use with **crosscore**.
These instructions assume that you are able to access the ``packer/`` directory within the crosscore source code.

For AWS, Azure, or GCP you can build a "base" image with `Docker <https://www.docker.com>`_ and GPU drivers installed,
and also `pinda <https://claughton.bitbucket.io>`_ so that containerised versions of various software packages
can easily be installed.

You can also build an "MD" image on top of the "base" image that has Ambertools and Gromacs pre-loaded.

************
Instructions
************

============
"Base" image
============

1. Edit ``xcore_base_provision.sh`` if desired.
2. Edit ``xcore_base_<provider>.json`` as needed.
3. In the case of GCP, you will need to create a project, create an associated service account with Owner role, 
   and download a .json file with your service account credentials (see :doc:`Installation <installation>` for further details) and then set ::

        export GOOGLE_CLOUD_KEYFILE_JSON=<path_to_credentials_file>
        
   and enable the Compute Engine API from the GCP console if this has not previously been enabled on this project.
        
4. Generate a new "base" image for your chosen provider::

        packer build xcore_base_<provider>.json

   This may take up to half an hour to complete.
5. Use the name of the new image that is generated to set the ``image_name`` value in your ``~/.xcore/config.yaml`` file

==========
"MD" image
==========

As above, but use ``xcore_MD_provision.sh`` and ``xcore_MD_<provider>.json``.
